class Dialog
  attr_accessor :lines, :name1, :name2, :img1, :img2, :prompt, :finished
  
  def initialize
    @lines = []
    @finished = false
  end
  
  def say(id, text)
    @lines << [id, text]
  end
  
  def getNextLine()
    @prompt += 1
    return @lines.at(@prompt-1)
  end
    
end