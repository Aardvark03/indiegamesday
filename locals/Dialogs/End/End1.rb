require_relative '../Dialog'

class End1 < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @name2 = "Parcival"
    @img1 = game.assetMgr.getImage("Dialog/player.png")
    @img2 = game.assetMgr.getImage("Dialog/parcival.png")
    
    @prompt = 0
    
    say(1, "The giant space Jelly")
    say(1, "Always wondered how these things manage to keep everything together")
    say(1, "And now they're going to space...")
    say(2, "Embrace the jelly!")
    say(1, "OK, let's get this done.")
  end
end