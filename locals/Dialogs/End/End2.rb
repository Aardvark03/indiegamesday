require_relative '../Dialog'

class End2 < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @img1 = game.assetMgr.getImage("Dialog/parcival.png")
    
    @prompt = 0
    
    say(1, "Smooth, just like a silk!")
    say(1, "And now up, up and away!")
  end
end