require_relative '../Dialog'

class Intro11 < Dialog
  def initialize(game)
    super()
    @name1 = "Voice"
    @img1 = game.assetMgr.getImage("Dialog/voice.png")
    
    @prompt = 0
    
    say(1, "On a little planet, everything was quiet.")
    say(1, "The three trees seen here absorbed every noise")
    say(1, "Even the ear-shattering drum sounds produced by villagemen, partying loudly just outside the cameras viewing area.")
    say(1, "It was quiet, until now...")
  end
end