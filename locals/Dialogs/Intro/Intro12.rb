require_relative '../Dialog'

class Intro12 < Dialog
  def initialize(game)
    super()
    @name1 = "Voice"
    @img1 = game.assetMgr.getImage("Dialog/voice.png")
    
    @prompt = 0
    
    say(1, "The crash destroyed most of the ship, except of a little chamber inside")
    say(1, "The next day the villagemen found the wreck and their chieftain declared it godsend.")
    say(1, "How else could it crash land there, without being heard?")
    say(1, "As I mentioned, the noise absorbing capabilities of the trees were just amazing.")
    
  end
end