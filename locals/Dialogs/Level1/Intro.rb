require_relative '../Dialog'

class Intro < Dialog
  def initialize(game)
    super()
    @name1 = "Voice"
    @name2 = "Radio"
    @img1 = game.assetMgr.getImage("Dialog/voice.png")
    @img2 = game.assetMgr.getImage("Dialog/voice.png")
    
    @prompt = 0
    
    say(1, "A long time nothing happened. From time to time the villagers would send someone to the wreck, to check if it was still there. Until now ...")
    say(2, "Ring, ring")
    say(2, "WAKE UP!")
    say(2, "COME HOME!")
    say(2, "love, Mom")
  end
end