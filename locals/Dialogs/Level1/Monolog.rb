require_relative '../Dialog'

class Monolog < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @img1 = game.assetMgr.getImage("Dialog/player.png")
    
    @prompt = 0
    
    say(1, "Damn, that sun is pretty bright.")
    say(1, "Without a body I will not survive for long.")
    say(1, "There seems to be somebody")
  end
end


