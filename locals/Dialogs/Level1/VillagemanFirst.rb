require_relative '../Dialog'

class VillagemanFirst < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @name2 = "Villageman"
    @img1 = game.assetMgr.getImage("Dialog/player.png")
    @img2 = game.assetMgr.getImage("Dialog/ned.png")
    
    @prompt = 0
    
    say(2, "That tickles.")
    say(1, "Seems this thing is intelligent enough to resist my control.")
    say(1, "You, fool ...")
    say(2, "... Ned")
    say(1, "Ned, bring me a creature that is worth being controlled. Bring me ...")
    say(1, "A lion!")
    
  end
end