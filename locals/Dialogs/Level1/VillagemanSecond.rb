class VillagemanSecond < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @name2 = "Ned"
    @img1 = game.assetMgr.getImage("Dialog/player.png")
    @img2 = game.assetMgr.getImage("Dialog/ned.png")
    
    @prompt = 0
    
    say(2, "I have got a rabbit in my hat.")
    say(1, "A rabbit? Fool!")
    say(1, "Well, it'll do it")
    say(2, "Are you a god?")
    say(1, "Well, you could say ...")
    say(2, "You have no nose. Tehee.")
    say(1, "Well ...")
    say(1, "Whatever. I need to get away from this planet")
    say(2, "Planet?")
    say(1, "Down here. I want up there.")
    say(2, "No one can fly higher than the clouds. Only the giant space jellyfish can")
    say(1, "Well, bring me to this jellyfish")
    say(2, "It's only a myth. Our shaman told me about it")
    say(1, "Well, then bring me to this shaman")
    say(2, "It's just a jump to the left.")
    say(2, "I mean right. Our village is right there. You can't miss it.")
  end
end