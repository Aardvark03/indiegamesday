require_relative '../Dialog'

class PlayerParcival < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @name2 = "Parcival"
    @img1 = game.assetMgr.getImage("Dialog/dialogplayer.png")
    @img2 = game.assetMgr.getImage("Dialog/dialogqualle.png")
    
    @prompt = 0
    
    say(2, "Yo! I came for the grove! I can feel it, yeah!")
	say(1, "I am glad you are here, giant space jellyfish.")
	say(2, "I am where the grove is.")
	say(1, "Can you take me above the clouds?")
	say(2, "I can take you everywhere unless its not groovy.")
	say(1, "The one groove is everywhere. You just have to feel it.")
	say(2, "Fine then. Let�s groove the space.")
	
end