require_relative '../Dialog'

class PlayerShaman1 < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @name2 = "Shaman"
    @img1 = game.assetMgr.getImage("Dialog/rabbit.png")
    @img2 = game.assetMgr.getImage("Dialog/shaman.png")
    
    @prompt = 0
    
    say(1, "Honoured Archpriest! I require your help!")
	say(2, "Divine coney! Incisored Lord! I will do all you require!")
	say(2, "Greetings, by the way, from Ganod, Erfor and The Crippled Daniel, to whom I just talked and who announced your imminent advent.")
	say(1, "Please return them my kindest regards the next time you converse with them. For now I am in desperate need of a way to fly above the clouds.")
	say(1, "Your conspecific told me about a giant space jellyfish who could suite my needs.")
	say(2, "Ah! Parcival! An impressive creature! I am afraid I have no knowledge of his current whereabouts, but I could possibly summon him to a nearby location.")
	say(1, "That would indeed very much please me.")
	say(2, "One hand washes the other. Acidentally I am in great need of a rabbit to complete our festive show-jumping course in honour of my oldest son, whose birthday it is.")
	say(1, "You want me to jump over hurdles?")
	say(2, "Actually watching that would be delightful for our entire festive congregation. And it's an easy task compared to what you want from me.")
	say(1, "Your salvation really seems quite urgent. But I will do as you wish, if you promise to call the giant space jellyfish afterwards.")
	say(2, "Don't worry about that, royal rabbit! You shall meet Parcival today.")
	say(2, "If you would please walk to the right into the parcours now...")
    
  end
end