require_relative '../Dialog'

class PlayerShaman2 < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @name2 = "Shaman"
    @img1 = game.assetMgr.getImage("Dialog/player.png")
    @img2 = game.assetMgr.getImage("Dialog/shaman.png")
    
    @prompt = 0
    
    say(2, "Oh no! Again one of those annoying and dangerous pop-ups. Go finish it off, divine rodent!")
	say(1, "I would consider doing that for you too, if I knew how.")
	say(2, "Just do what all rabbits do if they see something of about their size: Jump on it!")
	say(1, "Fine...")
    
  end
end
