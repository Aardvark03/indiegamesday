require_relative '../Dialog'

class PlayerShaman3 < Dialog
  def initialize(game)
    super()
    @name1 = "Player"
    @name2 = "Shaman"
    @img1 = game.assetMgr.getImage("Dialog/dialogplayer.png")
    @img2 = game.assetMgr.getImage("Dialog/dialogschamane.png")
    
    @prompt = 0
    
    say(2, "Good job, majestic bunny! You have rescued us all. Now I will call Parcival with a special piece of music.")
	say(1, "Go ahead, archpriest, I am very curious.")
  end
end