require_relative '../Dialog'

class ShamanNed < Dialog
  def initialize(game)
    super()
    @name1 = "Ned"
    @name2 = "Shaman"
    @img1 = game.assetMgr.getImage("Dialog/ned.png")
    @img2 = game.assetMgr.getImage("Dialog/shaman.png")
    
    @prompt = 0
    
    say(1, "Archpriest!")
	say(2, "You dare disturbing my meditation? I was most intimately chatting with our gods!")
	say(1, "I just talked to a god too!")
	say(1, "It emerged from our rusty sanctuary.")
	say(2, "How many times have we patiently listened to the products of your considerable imagination, ignoble Ned?")
	say(2, "For what reason should I believe you this time?")
	say(1, "Our divine Lord is right there! This most honourable rabbit has generously provided its body. Our Lord is somewhat discarnate.")
	say(2, "This must be your fortunate day! I would not believe you, but our gods just told me a rabbit was sent for our salvation.")
	say(2, "You shall be rewarded, Ned! Go to the church and take two wafers out of the little box in the second drawer on the left side of the altar.")
	say(1, " TWO! HOLY...")
	say(2, "One.")
	say(1, "...")
    
  end
end