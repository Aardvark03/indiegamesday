require_relative '../EntitySystem/Component'

class CollisionComponent < Component
  attr_accessor :type, :reaction, :static, :massive, :rect
  def initialize(type=:rect)
    @type = type
    @reaction = []
    @static = false
    @massive = true
    @rect = false
  end
end