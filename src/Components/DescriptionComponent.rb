require_relative '../EntitySystem/Component'

class DescriptionComponent < Component
  attr_accessor :name, :description, :intelligent, :action, :enemy
  def initialize
    @name = "No_Name"
    @description = nil
    @intelligent = false
    @action = :inspect
    @enemy = false
  end
end