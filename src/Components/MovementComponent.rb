require_relative '../EntitySystem/Component'
require_relative '../Utils/Vector2'

class MovementComponent
  attr_accessor :v, :v_max, :m, :f, :a, :target, :fixedBounds, :flying
  def initialize
    @v = Vector2.new(0.0, 0.0)
    @v_max = 50.0
    @a = Vector2.new(0.0, 0.0)
    @m = 1.0
    @f = Vector2.new(0.0, 0.0)
    @f_max = 2.0
    @target = :none
    @fixedBounds = true
    @flying = false
  end
end