require_relative '../EntitySystem/Component'
require_relative '../Utils/Vector2'

class PositionComponent < Component
  attr_accessor :pos, :size
  def initialize(pos=Vector2.new(0, 0), size=Vector2.new(0,0))
    @pos = pos
    @size = size
  end
end