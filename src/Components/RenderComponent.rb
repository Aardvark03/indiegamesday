require_relative '../EntitySystem/Component'

class RenderComponent < Component
  attr_accessor :img, :z, :angle, :rotate, :fontSize, :ui
  def initialize(img)
    @img = img
    @z = 0
    @angle = 0
    @rotate = false
    @fontSize = nil
    @ui = false
  end
end