require_relative '../EntitySystem/Component'

class ScriptComponent < Component
  attr_accessor :scripts
  def initialize
    @scripts = []
  end
end