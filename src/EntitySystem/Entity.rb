class Entity
  attr_accessor :components, :id, :doomed, :messages
  
  @@next_id = 1
  
  def initialize()
    @id = @@next_id
    @changed = true
    @doomed = false
    @@next_id += 1
    
    @messages = []
    @components = {}
  end
  
  def removeComponent(componentType)
    @components[componentType] = nil
  end
  
  def addComponent(component)
    if @components[component.class].nil?
      @components[component.class] = component
      @changed = true
    else
      puts "Entity #{@id}: Component bereits vorhanden"
    end
  end
  
  def has?(componentClass)
    if @components[componentClass].nil?
      false
    else
      true
    end
  end
  
  def getComponent(componentClass)
    @components[componentClass]
  end
  
  def changed?
    if @changed
      @changed = false
      true
    else
      false
    end
  end
  
end