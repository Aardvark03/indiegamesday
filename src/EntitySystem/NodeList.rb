class NodeList
  attr_accessor :nodes, :type
  
  def initialize(nodetype) 
    @nodes = {}
    @type = nodetype
  end
  
  def check(entity)
    @nodes.delete(entity) 
    return false if entity.doomed
    
    @type.componentTypes.each do |type|
      return false if not entity.has?(type)
    end
    
    @node = @type.new(entity)
    @nodes[entity] = @node
    true
  end
  
end