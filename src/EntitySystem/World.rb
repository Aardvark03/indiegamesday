require_relative 'Entity'
require_relative 'System'
require_relative 'NodeList'
require_relative '../Utils/Vector2'
require_relative '../Utils/Collision'
require_relative '../Utils/Grid/Grid'
require_relative '../Nodes/SelectionNode'
require_relative '../Nodes/CollisionNode'

class World
  attr_accessor :entities, :systems, :nodeLists, :grid
  
  include Collision
  
  def initialize(game)
    @game = game
    @entities = []
    @systems = {}
    @nodeLists = {}
    
    @grid = Grid.new(Vector2.new(100, 100), 10, 10)
    
    @changedEntities = []
  end
  
  def update(dt)

    @entities.each do |entity|
      if entity.doomed
        removeEntity(entity)
        next
      end
      @changedEntities.push(entity) if entity.changed?
    end
    
    refreshLists(@changedEntities) if not @changedEntities.empty?
    @changedEntities.clear
    
    @systems.each_value { |sys| sys.update(dt) }
    @grid.update(getNodeList(CollisionNode).nodes)
    
  end
  
  def addSystem(system)
    if @systems[system.class].nil?
      @systems[system.class] = system
      @systems[system.class].init
    end
  end
  
  def addEntity(entity)
    @entities.push(entity) if @entities.index(entity).nil?
  end
  
  def getEntity(entity)
    @entities.each {|entity| return entity if entity == entity}
    nil
  end
  
  def removeEntity(entity)
    entity.doomed = true
    
    @changedEntities.push(entity)
    refreshLists(@changedEntities)
    @entities.delete(entity)
  end
  
  def removeSystem(system)
    
    if system.class == Class
      systemclass = system
    else
      systemclass = system.class
    end
    
    @systems[systemclass].exit
    @systems.delete(systemclass)
    
  end
  
  def selectEntity(coords)
    selected = []
    entities = getNodeList(SelectionNode)
    entities.nodes.each_value do |node|
      position = node.getComponent(PositionComponent)
      pos = position.pos
      selected << node.entity if intersectionRectPoint(pos-position.size * 0.5, position.size, coords)
    end
    selected.sort {|a, b| a.getComponent(RenderComponent).z <=> b.getComponent(RenderComponent).z}
    return selected.first
  end

  def getNodeList(nodeType)
    if @nodeLists[nodeType].nil?
      @nodeLists[nodeType] = NodeList.new(nodeType)
      @entities.each { |entity| @nodeLists[nodeType].check(entity) }
    end
    return @nodeLists[nodeType]
  end
  
  def refreshLists(changedEntities)
    
    changedEntities.each do |entity|
      @nodeLists.each_value {|nodelist| nodelist.check(entity)}
    end
    
  end
end
      
      
