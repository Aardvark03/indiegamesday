require 'gosu'

require_relative 'Levels/Intro1'
require_relative 'EntitySystem/World'
require_relative 'Utils/EntityFactory'
require_relative 'Utils/AssetManager'
require_relative 'Systems/MovementSystem'
require_relative 'Components/MovementComponent'
require_relative 'Systems/RenderSystem'
require_relative 'Systems/CollisionSystem'
require_relative 'Systems/ScriptSystem'
require_relative 'Systems/MessagingSystem'

class Game
  attr_accessor :player, :camera, :world, :currentLevel, :factory, :window, :input, :locals, :assetMgr, :mousePointer, :camPos
  
  def initialize(window)
    raise "Window is no window" if not window.class.superclass == Gosu::Window
    
    @window = window
      
    @assetMgr = AssetManager.new(@window)
    @world = World.new(self)
    @factory = EntityFactory.new(@world, @window, @assetMgr, self)
    
    @world.addSystem(ScriptSystem.new(@world))
    @world.addSystem(MessagingSystem.new(@world))    
    @world.addSystem(MovementSystem.new(@world, self, @window)) 
    @world.systems[MovementSystem].side = true
    @world.addSystem(CollisionSystem.new(@world, self))
    @renderSystem = RenderSystem.new(@world, self)
    @world.addSystem(@renderSystem)
    
    changeLevel(Level1)
    
    @world.grid.updateSize(@currentLevel.size)
    
    @camPos = @camera.getComponent(PositionComponent).pos
    
    @input = {}
    
  end
  
  def update(dt)
    @world.update(dt)
    @input = {}
  end
  
  def render
      @window.scale(@window.scalingFactor, @window.scalingFactor) do
        @window.translate(-@camPos.x, -@camPos.y) do
        @renderSystem.render
      end
    end
  end
  
  def button_down(id)
    case id
      when Gosu::KbEscape
        exit
    end
    @input[id] = true
  end
  
  def button_down?(id)
    return @window.button_down?(id)
  end
  
  def changeLevel(levelClass)    
    if not @currentLevel.nil?
      @currentLevel.exit
    end
    @world.entities.each do |entity|
      next if entity == @player
      entity.doomed = true
    end
    
    level = levelClass.new(@window, self)
    raise "This is no Level!" if not level.kind_of?(Level)

    @currentLevel = level
    @currentLevel.setup
    @camPos = @camera.getComponent(PositionComponent).pos
    @world.grid.updateSize(@currentLevel.size)
    
  end
  
  def mousePos
    return Vector2.new(@window.mouse_x/@window.scalingFactor+@camPos.x, @window.mouse_y/@window.scalingFactor+@camPos.y)
  end
  
  def realCoords(gameCoords)
    if gameCoords.class == Vector2
      return gameCoords.dup / @window.scalingFactor
    else
      return gameCoords / @window.scalingFactor
    end
  end
  
end