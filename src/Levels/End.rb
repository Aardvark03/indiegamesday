require_relative 'Level'

require_relative '../Scripts/Triggers/End/DialogTrigger'
require_relative '../Scripts/Triggers/End/EndTrigger'

class End < Level
  attr_accessor :characters, :triggers, :ground
  def setup
    @name = "End"
    @world = @game.world
    @f = @game.factory
    
    @size = @f.createBackground("End/background.png")
    
    @game.mousePointer = @f.createMousePointer
    @game.player = @f.createPlayer(0, 750, :side)
    
    @game.camera = @f.createCamera(@game.player)
    
       
    @characters = {}
    @triggers = {}
    
    @characters[:player] = @game.player
    @triggers[:endTrigger] = @f.createTrigger(0, 0, 0, 0, EndTrigger, :none)
    @triggers[:dialogTrigger] = @f.createTrigger(1000, 500, 1000, 800, DialogTrigger, :collision)
    
    @game.world.systems[MovementSystem].side = true
    
    @ground = @f.createGround(900, @size.x, @size.y-900)
    
    @characters[:jellyfish] = createJellyfish
        
  end
  
  def createJellyfish
    jelly = @f.createNPC(1300, 800, :yellow)
    jelly.getComponent(MovementComponent).flying = true
    pos = jelly.getComponent(PositionComponent)
    rndr = jelly.getComponent(RenderComponent)
    description = jelly.getComponent(DescriptionComponent)
    mov = jelly.getComponent(MovementComponent)
    
    mov.fixedBounds = false
    
    rndr.img = @game.assetMgr.getImage("Creatures/jellyfish.png")
    
    pos.size = Vector2.new(rndr.img.width, rndr.img.height)
    description.description = "The giant space Jellyfish. I hope it doesn't sting"
    description.name = "Parcival"
    description.type = "Giant Space Jellyfish"
    
    return jelly
  end

  
  def exit
  end
  
end