require_relative 'Level'

require_relative '../../locals/Dialogs/Intro/Intro11'
require_relative '../Scripts/Triggers/Intro/Intro1Trigger'

class Intro1 < Level
  attr_accessor :dialog, :camera
  def setup
    @name = "Intro1"
    @world = @game.world
    @f = @game.factory
    
    @size = @f.createBackground("Intro/background.png")
    
    @game.mousePointer = @f.createMousePointer
    @game.camera = @f.createCamera(Vector2.new(600, 500))
    @game.camera.getComponent(MovementComponent).targetType = :fixed
    
    @dialog = Intro11.new(@game)
    @f.createDialogWindow(@dialog)
    
    @trigger = @f.createTrigger(0, 0, 0, 0, Intro1Trigger, :none)

  end
  
  def createShip
    ship = @f.createNPC(350, -100, :yellow)
    
    @pos = ship.getComponent(PositionComponent)
    @render = ship.getComponent(RenderComponent)
    @render.img = @game.assetMgr.getImage("Intro/ship.png")
    
    @pos.size.x = @render.img.width
    @pos.size.y = @render.img.height
    
    ship.getComponent(MovementComponent).target = Vector2.new(350, 1000)
    
    return ship
  end
  
  def exit
  end
  
end