require 'gosu'

require_relative 'Level'

require_relative '../Scripts/Triggers/Level1/Begin1Trigger'
require_relative '../Scripts/Triggers/Level1/VillagemanTrigger'
require_relative '../Scripts/Triggers/Level1/End1Trigger'

class Level1 < Level
  attr_accessor :characters, :trigger, :name
  
  def setup
    @name = "Level1"
    @world = @game.world
    
    @f = @game.factory
    
    @characters = {}
    @triggers = {}
    
    @size = @f.createBackground("Level1/hintergrund1.png")
    @ground = @f.createGround(900, @size.x, @size.y-900)
    
    @game.mousePointer = @f.createMousePointer
    @game.player = @f.createPlayer(300, 300)
    @camera = @f.createCamera(@game.player)
    @game.camera = @camera
    
    @characters[:player] = @game.player
    
    @f.createSun(true)
    
    createNed
    
    
    @f.createBGImage(1530, 970, @game.assetMgr.getImage("Plants/flower3.png"), 6)
    
    @triggers[:begin] = @f.createTrigger(0, 0, 0, 0, Begin1Trigger, :none)
    @triggers[:end] = @f.createTrigger(1800, 100, 200, 900, End1Trigger, :collision)
  end
  
  def exit
  end
  
  def createNed
    ned = @f.createNPC(700, 800, "Creatures/villageman1.png", 4)
    descr = ned.getComponent(DescriptionComponent) 
    descr.description = "His name is Ned."
    descr.action = :control
    descr.intelligent = true
    
    scripts = ned.getComponent(ScriptComponent).scripts 
    scripts << VillagemanTrigger.new(ned, @game)
    @characters[:villageman] = ned
  end
  
end