require 'gosu'

require_relative 'Level'

require_relative '../Scripts/Triggers/Level2/Begin2Trigger'
require_relative '../Scripts/Triggers/Level2/End2Trigger'
require_relative '../Scripts/Triggers/Level2/ApplauseTrigger'
require_relative '../Scripts/Triggers/Level2/AchievementTrigger'


class Level2 < Level
  attr_accessor :characters, :trigger, :name
  
  def setup
    @name = "Level2"
    @world = @game.world
    
    @game.player.doomed = false

    
    @f = @game.factory
    
    @characters = {}
    @triggers = {}
    
    @size = @f.createBackground("Level2/background.png")
    @ground = @f.createGround(900, @size.x, @size.y-900)
    
    @game.mousePointer = @f.createMousePointer

    @camera = @f.createCamera(@game.player)
    @game.camera = @camera
    
    @characters[:player] = @game.player
    @game.player.getComponent(PositionComponent).pos = Vector2.new(0, 800)
    @game.world.addEntity(@game.player)
    
    createNed
    createShaman
    
    @f.createSun(false)

    
    @f.createNPC(900, 800, "Creatures/villageman2.png", 3)
    
    @triggers[:begin] = @f.createTrigger(0, 0, 0, 0, Begin2Trigger, :none)
   # @triggers[:end] = @f.createTrigger(3950, 100, 200, 900, End2Trigger, :collision)
  
    createHurdle(1560, 750, 80, 400)
    createHurdle(2160, 730, 80, 400)
    createHurdle(2750, 750, 80, 400)
    
    @f.createTrigger(1650, 0, 100, 1080, ApplauseTrigger, :collision)
    @f.createTrigger(2250, 0, 100, 1080, ApplauseTrigger, :collision)
    @f.createTrigger(2950, 0, 100, 1080, ApplauseTrigger, :collision)
    
    @triggers[:achievementTrigger] = @f.createTrigger(3100, 0, 100, 1080, AchievementTrigger, :collision)

  end
  
  def exit
    @song.stop
  end
  
  def createNed
    ned = @f.createNPC(-150, 900, "Creatures/villageman1.png", 4.1)
    descr = ned.getComponent(DescriptionComponent) 
    descr.description = "It's Ned again."
    descr.action = :inspect
    descr.intelligent = true
    
    @characters[:ned] = ned
    
    return ned
  end
  
  def createVillageman
    man = @f.createNPC(900, 800, "Creatures/villageman2.png", 3)
    descr = man.getComponent(DescriptionComponent)
    descr.descr = "Another one. He looks more frightend than important."
    descr.action = :inspect
  end
  
  def createShaman
    shaman = @f.createNPC(400, 800, "Creatures/shaman.png", 4)
    
    descr = shaman.getComponent(DescriptionComponent) 
    descr.description = "He looks important"
    descr.action = :talk
    descr.intelligent = true
    
    @characters[:shaman] = shaman
    
    return shaman
  end
  
  def createHurdle(x, y, width, height)
    hurdle = Entity.new
    
    pos = PositionComponent.new
    
    pos.pos.x = x
    pos.pos.y = y
    pos.size.x = width
    pos.size.y = height
    
    coll = CollisionComponent.new
    coll.static = true
    
    hurdle.addComponent(pos)
    hurdle.addComponent(coll)
    
    @game.world.addEntity(hurdle)
    
    return hurdle
  end
end