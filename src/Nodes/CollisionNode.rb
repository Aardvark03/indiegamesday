require_relative '../EntitySystem/Node'
require_relative '../Components/CollisionComponent'
require_relative '../Components/PositionComponent'

class CollisionNode
  attr_accessor :entity, :components
  
  include Node
  @@componentTypes = [PositionComponent, CollisionComponent]
  def initialize(entity)
    @entity = entity
    @components = {}
    
    @components[PositionComponent] = entity.getComponent(PositionComponent)
    @components[CollisionComponent] = entity.getComponent(CollisionComponent)
  end
  
  def self.componentTypes
    @@componentTypes
  end
end