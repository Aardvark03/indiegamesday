require_relative '../EntitySystem/Node'
require_relative '../Components/ScriptComponent'
require_relative '../Components/MessagingComponent'

class MessagingNode
  attr_accessor :entity, :components
  
  include Node
  @@componentTypes = [ScriptComponent, MessagingComponent]
  def initialize(entity)
    @entity = entity
    @components = {}
    
    @components[ScriptComponent] = entity.getComponent(ScriptComponent)
    @components[MessagingComponent] = entity.getComponent(MessagingComponent)
  end
  
  def self.componentTypes
    @@componentTypes
  end
end