require_relative '../EntitySystem/Node'
require_relative '../Components/PositionComponent'
require_relative '../Components/MovementComponent'
require_relative '../Components/RenderComponent'

class MoveNode
  attr_accessor :entity, :components
  
  include Node
  @@componentTypes = [PositionComponent, MovementComponent]
  def initialize(entity)
    @entity = entity
    @components = {}
    
    @components[PositionComponent] = entity.getComponent(PositionComponent)
    @components[MovementComponent] = entity.getComponent(MovementComponent)
    
    @components[RenderComponent] = entity.getComponent(RenderComponent) if (entity.has?(RenderComponent))
  end
  
  def self.componentTypes
    @@componentTypes
  end
end