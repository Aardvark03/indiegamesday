require_relative '../EntitySystem/Node'
require_relative '../Components/PositionComponent'
require_relative '../Components/RenderComponent'

class RenderNode
  attr_accessor :entity, :components
  
  include Node
  @@componentTypes = [PositionComponent, RenderComponent]
  def initialize(entity)
    @entity = entity
    @components = {}
    
    @components[PositionComponent] = entity.getComponent(PositionComponent)
    @components[RenderComponent] = entity.getComponent(RenderComponent)
  end
  
  def self.componentTypes
    @@componentTypes
  end
end