require_relative '../EntitySystem/Node'
require_relative '../Components/ScriptComponent'

class ScriptNode
  attr_accessor :entity, :components
  
  include Node
  @@componentTypes = [ScriptComponent]
  def initialize(entity)
    @entity = entity
    @components = {}
    
    @components[ScriptComponent] = entity.getComponent(ScriptComponent)
  end
  
  def self.componentTypes
    @@componentTypes
  end
end