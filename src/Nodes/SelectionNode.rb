require_relative '../EntitySystem/Node'
require_relative '../Components/DescriptionComponent'
require_relative '../Components/PositionComponent'

class SelectionNode
  attr_accessor :entity, :components
  
  include Node
  @@componentTypes = [DescriptionComponent, PositionComponent]
  def initialize(entity)
    @entity = entity
    @components = {}
    
    @components[DescriptionComponent] = entity.getComponent(DescriptionComponent)
    @components[PositionComponent] = entity.getComponent(PositionComponent)
  end
  
  def self.componentTypes
    @@componentTypes
  end
end