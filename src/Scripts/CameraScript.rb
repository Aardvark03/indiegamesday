require_relative '../Components/PositionComponent'

require_relative '../Scripts/Script'

class CameraScript < Script
  def initialize(entity, game)
    super(entity, game)
    
    @mov = @entity.getComponent(MovementComponent)
    @pos = @entity.getComponent(PositionComponent)
  end
  def update(dt)
    @playerPos = @game.currentLevel.characters[:player].getComponent(PositionComponent)
    
    @mov.target = @playerPos.pos - @pos.size * 0.5 * @game.window.scalingFactor
  end
end