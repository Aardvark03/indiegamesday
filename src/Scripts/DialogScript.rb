require_relative 'Script'
require_relative '../Components/PositionComponent'
require_relative '../Components/RenderComponent'

require_relative '../Utils/Collision'
require_relative '../Utils/Vector2'

class DialogScript < Script
  include Collision
  
  def initialize(entity, game, dialog)
    super(entity, game)
    @dialog = dialog
    
    @img1 = dialog.img1
    @img2 = dialog.img2
    @name1 = dialog.name1
    @name2 = dialog.name2
    
    @pos = @entity.getComponent(PositionComponent)
    
    @portrait = @game.factory.createBGImage(70, @pos.pos.y, @img1, 11)
    @image = @portrait.getComponent(RenderComponent)
    @image.z = 10
    @image.ui = true
    
    
    raise "DialogWindow Position is missing" if @pos.nil?
    
    line = @dialog.getNextLine
    
    if line.nil?
      @entity.doomed = true
          
    else
      if line[0] == 1
        @image.img = @img1
        @currentText = @game.factory.createDialogText(@pos.pos.x+@image.img.width, @pos.pos.y, @pos.size.x-500, line[1], @name1)
      elsif line[0] == 2
        @image.img = @img2
        @currentText = @game.factory.createDialogText(@pos.pos.x+@image.img.width, @pos.pos.y, @pos.size.x, line[1], @name2)
      end
    end 
  end
  
  def update(dt)
    input = @game.input
    
    if input[Gosu::MsLeft]
      if intersectionRectPoint(@pos.pos-@pos.size*0.5, @pos.size, Vector2.new(@game.window.mouse_x/@game.window.scalingFactor, @game.window.mouse_y/@game.window.scalingFactor))
        @currentText.doomed = true
        line = @dialog.getNextLine
        
        if line.nil?
          @dialog.finished = true
          @entity.doomed = true
          @portrait.doomed = true          
        else
          if line[0] == 1
            @image.img = @img1
            @currentText = @game.factory.createDialogText(@pos.pos.x+50+@image.img.width, @pos.pos.y, @pos.size.x, line[1], @name1)
          elsif line[0] == 2
            @image.img = @img2
            @currentText = @game.factory.createDialogText(@pos.pos.x+50+@image.img.width, @pos.pos.y, @pos.size.x, line[1], @name2)
          end
        end
      end
    end
  end
  
end