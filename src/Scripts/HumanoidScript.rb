require_relative 'Script'

class HumanoidScript < Script
  
  def initialize(entity, game)
    super(entity, game)
    @messages = @entity.getComponent(MessagingComponent)
    
    @mov = @entity.getComponent(MovementComponent)
    @pos = @entity.getComponent(PositionComponent)
    
    @freeze = false
    
   
  end
  
  def update(dt)
    @moved = false
    @onGround = false
    
    @messages.recieved.each do |message|
      case message.type
        when :freeze
          @freeze = true
          @mov.v.x = 0
          @mov.v.y = 0
        when :unfreeze
          @freeze = false
        when :moveTo
          @mov.target = message.data[:target]
        when :stop
          @mov.target = :none
          @mov.v = Vector2.new(0, 0)
        when :move
          move(message.data[:direction]) if not @freeze
        when :collision
         @onGround = true if message.data[:side] == :bottom
        when :stop
          @mov.target = :none
          @target = nil
      end
    end
    @messages.recieved.clear
    
    if not @moved and @onGround
      @mov.v = Vector2.new(0, 0)
    end

  end
  
  def move(direction)        
    case direction
      when :left
        @moved = true
        @mov.f += Vector2.new(-1000, 0) 
      when :right
        @moved = true
        @mov.f += Vector2.new(1000, 0)
      when :up
        if @onGround
          @moved = true
          @mov.f += Vector2.new(0, -20000)
        end
    end
  end
  
  
end