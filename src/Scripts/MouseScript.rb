require_relative '../Components/PositionComponent'
require_relative '../Components/ScriptComponent'
require_relative '../Components/RenderComponent'

require_relative '../Scripts/Script'

class MouseScript < Script
  def initialize(entity, game)
    super(entity, game)
    @images = {}
    @images[:inspect] = @game.assetMgr.getImage("Mouse/inspect.png")
    @images[:control] = @game.assetMgr.getImage("Mouse/control.png")
    @images[:talk] = @game.assetMgr.getImage("Mouse/talk.png")
    @images[:normal] = @game.assetMgr.getImage("Mouse/normal.png")
    
    @render = @entity.getComponent(RenderComponent)
    
  end
  def update(dt)
    
    pos = @entity.getComponent(PositionComponent).pos
    pos.x = @game.mousePos.x
    pos.y = @game.mousePos.y 
  
    if @selected.nil? or @selected == @game.currentLevel.characters[:player]
      @render.img = @images[:normal]
    else
      @render.img = @images[@selected.getComponent(DescriptionComponent).action]
    end
  end
end