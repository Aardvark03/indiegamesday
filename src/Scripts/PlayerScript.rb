require 'gosu'

require_relative 'Script'

require_relative '../Utils/Vector2'

class PlayerScript < Script
  attr_accessor :mode
  
  def initialize(entity, game)
    super(entity, game)
    
    @messages = @entity.getComponent(MessagingComponent)
    
    @currentSentence = nil
    
    @pos = @entity.getComponent(PositionComponent)
    
    @ghost = true

  end
  def update(dt)
    input = @game.input     
    
    if @game.button_down?(Gosu::KbW)
      message = Message.new(@entity, @entity, :move)
      message.data[:direction] = :up
      @messages.recieved << message
    end

    if @game.button_down?(Gosu::KbS)
      message = Message.new(@entity, @entity, :move)
      message.data[:direction] = :down
      @messages.recieved << message
    end
    
    if @game.button_down?(Gosu::KbA)
      message = Message.new(@entity, @entity, :move)
      message.data[:direction] = :left
      @messages.recieved << message
    end
    
    if @game.button_down?(Gosu::KbD)
      message = Message.new(@entity, @entity, :move)
      message.data[:direction] = :right
      @messages.recieved << message
    end
    
    
    
    if input[Gosu::MsLeft]
      selected = @world.selectEntity(@game.mousePos)
      
      if not selected.nil? and not selected == @entity
        case selected.getComponent(DescriptionComponent).action
          when :inspect
            inspect(selected)
          when :control
            takeControl(selected)
        end
      end
    end
    
    if input[Gosu::KbSpace]
      @messages.recieved = Message.new(@entity, @entity, :attack)
    end
    
  end
  
  def inspect(victim)
    say(victim.getComponent(DescriptionComponent).description)
  end
  
  def takeControl(victim)
    dist = (victim.getComponent(PositionComponent).pos - @pos.pos).length
    
    if dist > 300
      say("I'm too far away")
      return
    end
    @messages.send << Message.new(@entity, victim, :takingControl)
    
    if victim.getComponent(DescriptionComponent).intelligent
      say("I can't control a strong mind")
      return
    end
    
    
    scripting = victim.getComponent(ScriptComponent)
    
    
    
    scripting.scripts[scripting.scripts.size] = scripting.scripts[0] if not scripting.scripts[0].nil?
    scripting.scripts[0] = self
    
    if @ghost
      @entity.doomed = true
    else
      @entity.getComponent(ScriptComponent).scripts.delete(self)
    end
    @entity = victim
    @ghost = false
    @game.player = @entity
    @game.currentLevel.characters[:player] = @entity

    @pos = @entity.getComponent(PositionComponent)

    
    @messages = @entity.getComponent(MessagingComponent)
  end
  
  def moveOut
    return if @ghost
    
    @ghost = true

    @messages.recieved << Message.new(@entity, @entity, :stop)
    
    entity = @game.factory.createPlayer(@pos.pos.x+300, @pos.pos.y)
    
    @messages = entity.getComponent(MessagingComponent)
    
    entity.getComponent(ScriptComponent).scripts[0] = self
    
    @entity.getComponent(ScriptComponent).scripts.delete(self)
    
    @entity = entity
    @pos = @entity.getComponent(PositionComponent)

    @game.player = @entity
  end
  
  def say(string)
    pos = @pos.pos
    if not @currentSentence.nil?
      @currentSentence.doomed = true
    end
    @currentSentence = @game.factory.createSentence(pos.x+30, pos.y-50, string, @entity)
  end
end