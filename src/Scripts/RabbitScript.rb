require 'gosu'

require_relative 'Script'

class RabbitScript < Script
  
  def initialize(entity, game)
    super(entity, game)
    
    @mov = @entity.getComponent(MovementComponent)
    @pos = @entity.getComponent(PositionComponent)
    @render = @entity.getComponent(RenderComponent)
    @messages = @entity.getComponent(MessagingComponent)

    
    @freeze = false
    @onGround = false
    @moved = true
    
    @target = nil
    
    @images = []
    @images << Gosu::Image.load_tiles(@game.window, "Assets/Creatures/rabbit1.png", -1, -2, true)
    @images << Gosu::Image.load_tiles(@game.window, "Assets/Creatures/rabbit2.png", -1, -2, true)
    @position = 0
    @dir = 0
    @render.img = @images[@position][0]
  end
  
  def update(dt)
    @onGround = false
    @moved = false
        
    @messages.recieved.each do |message|
      case message.type
        when :freeze
          @freeze = true
          @mov.v.x = 0
          @mov.v.y = 0
        when :unfreeze
          @freeze = false
        when :moveTo
          @target = message.data[:target]
        when :collision
         @onGround = true if message.data[:side] == :bottom
        when :stop
          @mov.target = :none
          @target = nil
        when :move
          move(message.data[:direction]) if not @freeze
      end
    end
    @messages.recieved.clear

    if not @moved and @onGround
      @mov.v = Vector2.new(0, 0)
    end
    
    
    if @onGround
      @position = 0
    else
      @position = 1
    end
    
    if @mov.v.x > 0
      @dir = 0
    elsif @mov.v.x < 0
      @dir = 1
    end
    
    @render.img = @images[@position][@dir]

    
    if not @target.nil?
      if @pos.pos.x.between?(@target.x - 20, @target.x + 20)
        @target = nil
      else
        if @target.x > @pos.pos.x
          move(:right)
        else
          move(:left)
        end
      end
    end
  end
  
  def move(direction)
    case direction
      when :left
        if @onGround
          @moved = true
          @mov.v.x = 0 if @mov.v.x > 0
          @mov.f += Vector2.new(-4000, -15000) 
        end
      when :right
        if @onGround
          @mov.v.x = 0 if @mov.v.x < 0
          @moved = true
          @mov.f += Vector2.new(4000, -15000)
        end
      when :up
        if @onGround
          @moved = true
          @mov.f += Vector2.new(0, -20000)
        end
    end
  end
  
  
end