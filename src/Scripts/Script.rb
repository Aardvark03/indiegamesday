class Script
  attr_accessor :active
  
  def initialize(entity, game)
    @game = game
    @entity = entity
    @window = game.window
    @world = game.world
    @active = true
  end
  
  
  def update(dt)
  end  
end