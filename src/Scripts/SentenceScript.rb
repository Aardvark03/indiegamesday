require_relative 'Script'

require_relative '../Components/MovementComponent'
require_relative '../Components/PositionComponent'

class SentenceScript < Script
  def initialize(entity, world, speaker)
    super(entity, world)
    @speaker = speaker
    @lifeTime = 3.0
    @time = 0
  end
  
  def update(dt)
    @time += dt
    
    @entity.getComponent(PositionComponent).pos = @speaker.getComponent(PositionComponent).pos+Vector2.new(30, -80)
    
    @entity.doomed = true if @lifeTime < @time
  end
  
  
end