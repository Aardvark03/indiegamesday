require_relative 'Script'

class SunScript < Script
  
  def initialize(entity, game, cloudless)
    super(entity, game)
    
    @clouds = []
    
    @cloudless = cloudless
    @type = [:small, :normal, :big]
    4.times {@clouds << @game.factory.createCloud(rand(@game.currentLevel.size.x), rand(100), @type.sample)} if not @cloudless
    @timer = 0
    
  end
  
  def update(dt)
    @clouds.each do |cloud|
      if cloud.getComponent(PositionComponent).pos.x < 0 - cloud.getComponent(PositionComponent).size.x
        cloud.doomed = true
        @clouds.delete(cloud)
      end
    end
      
    @timer += dt
      
    if @timer > 20
      @timer = 0
      @clouds << @game.factory.createCloud(1920+300, rand(100), @type.sample) if not @cloudless
    end
  end
end