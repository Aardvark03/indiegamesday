require_relative '../../Script'

require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'

require_relative '../../../../locals/Dialogs/End/End1'


class DialogTrigger < Script
  def initialize(entity, game)
    super(entity, game)

    @messages = @entity.getComponent(MessagingComponent)
    
    @talking = false
    @executed = false
  end
  
  def update(dt)
    @messages.recieved.each do |message|
      case message.type
        when :collision
          if message.data[:entity] == @game.currentLevel.characters[:player] and not @executed
            @executed = true
            @talking = true
            @dialog = End1.new(@game)
            @game.factory.createDialogWindow(@dialog)
            @messages.send << Message.new(@entity, @game.currentLevel.characters[:player], :freeze)
          end
      end
      
    end
    @messages.recieved.clear
      
    if @talking
      if @dialog.finished
        @send << Message.new(@entity, @game.currentLevel.characters[:player], :unfreeze)
      end
    end
  end
end