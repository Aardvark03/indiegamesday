require_relative '../../Script'

require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'

require_relative '../../../../locals/Dialogs/End/End2'


class EndTrigger < Script
  def initialize(entity, game)
    super(entity, game)

    @messages = @entity.getComponent(MessagingComponent)
    @sent = false
    @created = false
  end
  
  def update(dt)
    @messages.recieved.clear
    
    if @game.currentLevel.characters[:jellyfish] == @game.player and not @speaking
      @speaking = true
      
      @dialog = End2.new(@game)
      @game.factory.createDialogWindow(@dialog)
      @messages.send << Message.new(@entity, @game.player, :freeze)
    end
      
    if @speaking and not @sent
      if @dialog.finished
        @sent = true
        message = Message.new(@entity, @game.player, :moveTo)
        message.data[:target] = Vector2.new(@game.player.getComponent(PositionComponent).pos.x, -500)
        @messages.send << message
      end
    end
    
    if @game.player.getComponent(PositionComponent).pos.y < -200 and not @created
      @created = true
      
      createString
      
    end
  end
  
  def createString
    string = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = @game.window.resolution.x * 0.2
    pos.pos.y = @game.window.resolution.y * 0.2
    
    rndr = RenderComponent.new("The End")
    rndr.z = 9
    rndr.fontSize = :big
    rndr.ui = true
    
    string.addComponent(pos)
    string.addComponent(rndr)
    
    @world.addEntity(string)
    
    return string
  end
end