require_relative '../../Script'

require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'

require_relative '../../../../locals/Dialogs/Intro/Intro12'
require_relative '../../../Levels/Level1'
require_relative '../../../Levels/End'


class Intro1Trigger < Script
  def initialize(entity, game)
    super(entity, game)
    
    @dialog1 = @game.currentLevel.dialog
    
    @shipCreated = false
    @crashed = false
    
    @recieved = @entity.getComponent(MessagingComponent).recieved
  
  end
  
  def update(dt)
    @recieved.clear
    
    if @dialog1.finished and not @shipCreated
      @shipCreated = true
      @game.player = @game.currentLevel.createShip
      @mov = @game.player.getComponent(MovementComponent)
      @pos = @game.player.getComponent(PositionComponent)
      @render = @game.player.getComponent(RenderComponent)
    end
    
    return if not @shipCreated
    
    if @pos.pos.y > 450 and not @crashed
      @crashed = true
      @mov.target = @pos.pos
      @mov.v = Vector2.new(0, 0)
      @render.img = @game.assetMgr.getImage("Intro/boom.png")
      @timer = 0
    end
    
    if not @timer.nil?
      if @timer < 0.5
        @timer += dt
      else
        @render.img = @game.assetMgr.getImage("Intro/ship-broken.png")
        @dialog2 = Intro12.new(@game)
        @game.factory.createDialogWindow(@dialog2)
        @timer = nil
      end
    end
    
    if not @dialog2.nil?
      if @dialog2.finished
        @game.changeLevel(End)
      end
    end
  end
end