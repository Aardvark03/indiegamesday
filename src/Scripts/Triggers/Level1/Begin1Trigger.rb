require_relative '../../Script'

require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'

require_relative '../../../../locals/Dialogs/Level1/Intro'
require_relative '../../../../locals/Dialogs/Level1/Monolog'

class Begin1Trigger < Script
  def initialize(entity, game)
    super(entity, game)

    @dialog = Intro.new(@game)
    @dialog2 = Monolog.new(@game)
    @render = @game.currentLevel.characters[:player].getComponent(RenderComponent)
    @game.currentLevel.characters[:player].removeComponent(RenderComponent)
    @messages = @entity.getComponent(MessagingComponent)
    @game.factory.createDialogWindow(@dialog)
    @messages.send << Message.new(@entity, @game.currentLevel.characters[:player], :freeze)
    @monolog = false
  end
  
  def update(dt)
    @messages.recieved.each do |message|
      case message.type
        when :destroy
          @entity.doomed = true
      end
    end
    
    @messages.recieved.clear
    if @dialog.finished and not @monolog
      @monolog = true
      @game.currentLevel.characters[:player].addComponent(@render)
      @game.factory.createDialogWindow(@dialog2)
    end
    
    if @dialog2.finished
      @messages.send << Message.new(@entity, @game.currentLevel.characters[:player], :unfreeze)
      @messages.send << Message.new(@entity, @entity, :destroy)
    end

  end
    
end