require_relative '../../Script'

require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'

require_relative '../../../Levels/Level2'

class End1Trigger < Script
  def initialize(entity, game)
    super(entity, game)
    @messages = @entity.getComponent(MessagingComponent)
  end
  
  def update(dt)
    @messages.recieved.each do |message|
      case message.type
        when :collision
          if message.data[:entity] == @game.currentLevel.characters[:player]
            if message.data[:entity].getComponent(ScriptComponent).scripts.each do |script|
              if script.instance_of?(RabbitScript)
                @game.changeLevel(Level2)
              end
            end
          else
            message.data[:entity].getComponent(ScriptComponent).scripts.each do |script|
              if script.instance_of?(PlayerScript)
                script.say("I should get a proper body first")
              end
            end
          end
        end
      end
    end
    @messages.recieved.clear
  end
end