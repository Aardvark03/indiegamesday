require_relative '../../Script'

require_relative '../../../../locals/Dialogs/Level1/VillagemanFirst'
require_relative '../../../../locals/Dialogs/Level1/VillagemanSecond'


require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'

class VillagemanTrigger < Script
  def initialize(entity, game)
    super(entity, game)
    scripts = @entity.getComponent(ScriptComponent).scripts
    
    @messages = @entity.getComponent(MessagingComponent)
    
    @executed = false
    @finished = false
    
    @dialog = VillagemanFirst.new(@game)
    @dialog2 = VillagemanSecond.new(@game)
    
    scripts.delete(self)
    script = scripts[0]
    scripts[0] = self
    scripts[scripts.size] = script
    @characters = @game.currentLevel.characters
    
    @villagemanPos = @entity.getComponent(PositionComponent)
  end
  
  def update(dt)
    @messages.recieved.each do |message|
      case message.type
        when :takingControl
          execute
        when :remove
          @entity.getComponent(ScriptComponent).scripts.delete(self)
      end
    end
    @messages.recieved.clear
    
    return if not @executed
    
    if @dialog.finished and not @finished
      @finished = true
      @game.factory.createDialogWindow(@dialog2)
      @characters[:rabbit] = @game.factory.createCreature(1000, 500, :rabbit)
      @characters[:villageman].getComponent(DescriptionComponent).action = :inspect
    end
      
    if @dialog2.finished
      @messages.send << Message.new(@entity, @characters[:player], :unfreeze)
      @messages.send << Message.new(@entity, @entity, :remove)
    end
  end
  
  def execute
    @executed = true      
    @game.factory.createDialogWindow(@dialog)
    @messages.send << Message.new(@entity, @characters[:player], :freeze)
  end
end