require_relative '../../Script'
require_relative '../../../Utils/Message'
require_relative '../../../../locals/Dialogs/Level2/PlayerShaman2'

class AchievementTrigger < Script
  def initialize(entity, game)
    super(entity, game)
    
    @messaging = @entity.getComponent(MessagingComponent)
    
    @dialog = PlayerShaman2.new(@game)
    
    @executed = false
    @talking = false
    @finished = false
  end
  
  
  def update(dt)
    @messaging.recieved.each do |message|
      case message.type
        when :collision
          if message.data[:entity] == @game.player
            execute if not @executed
          end
        when :destroy
          @entity.doomed = true
      end
    end
    return if not @executed
    
    if @achievement.getComponent(MovementComponent).target == :none and not @talking
      @talking = true
      @game.factory.createDialogWindow(@dialog)
    end
    
    if @talking and @dialog.finished
      @messaging.send << Message.new(@entity, @game.player, :unfreeze)
      @messaging.send << Message.new(@entity, @entity, :destroy)
    end
  end
  
  def execute
    @executed = true
    @messaging.send << Message.new(@entity, @game.player, :freeze)
    @messaging.send << Message.new(@entity, @game.player, :stop)
    
    @achievement = createAchievement
    
  end
  
  def createAchievement
    achievement = Entity.new
    
    pos = PositionComponent.new
    rndr = RenderComponent.new(@game.assetMgr.getImage("Level2/achievement.png"))
        
    pos.size.x = rndr.img.width
    pos.size.y = rndr.img.height
    
    pos.pos.x = 4000 - 1.5*pos.size.x
    pos.pos.y = 1080 + 0.5*pos.size.y
    
    coll = CollisionComponent.new
    coll.static = true
    
    descr = DescriptionComponent.new
    descr.description = "This things seem to be everywhere"
    descr.enemy = true
    descr.action = :inspect
    
    mov = MovementComponent.new
    mov.v_max = 30
    mov.target = Vector2.new(pos.pos.x, 1080-0.5*pos.size.y)
    
    achievement.addComponent(mov)
    achievement.addComponent(pos)
    achievement.addComponent(rndr)
    achievement.addComponent(descr)
    achievement.addComponent(coll)
    
    @game.world.addEntity(achievement)
    
    return achievement
 
  end
end