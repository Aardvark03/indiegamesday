require_relative '../../Script'
require_relative '../../../Utils/Message'

class ApplauseTrigger < Script
  def initialize(entity, game)
    super(entity, game)
    
    @messaging = @entity.getComponent(MessagingComponent)
  end
  
  
  def update(dt)
    @messaging.recieved.each do |message|
      case message.type
        when :collision
          if message.data[:entity] == @game.player
            @game.assetMgr.getSample("hopp.ogg").play
            @messaging.send << Message.new(@entity, @entity, :destroy)
          end
        when :destroy
          @entity.doomed = true
      end
    end
  end
end