require_relative '../../Script'

require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'

require_relative '../../../../locals/Dialogs/Level2/ShamanNed'
require_relative '../../../../locals/Dialogs/Level2/PlayerShaman1'

class Begin2Trigger < Script
  def initialize(entity, game)
    super(entity, game)

    @dialog = ShamanNed.new(@game)
    @dialog2 = PlayerShaman1.new(@game)
    
    @characters = @game.currentLevel.characters
    
    @messages = @entity.getComponent(MessagingComponent)
    @messages.send << Message.new(@entity, @game.currentLevel.characters[:player], :freeze)
    message = Message.new(@entity, @characters[:ned], :moveTo)
    message.data[:target] = Vector2.new(700, 900)
    @messages.send << message
    
    @talking = false
    @finished = false
    @arrived = false
  end
  
  def update(dt)
    @messages.recieved.each do |message|
      case message.type
        when :destroy
          @entity.doomed = true
      end
    end
    
    @messages.recieved.clear
    
    if @characters[:ned].getComponent(MovementComponent).target != :none
      if (@characters[:ned].getComponent(PositionComponent).pos - @characters[:ned].getComponent(MovementComponent).target).length < 300 and not @talking
        @messages.send << Message.new(@entity, @characters[:ned], :stop)
        @talking = true
        @game.factory.createDialogWindow(@dialog)
      end
    end
    
    if @dialog.finished and not @finished
      @finished = true
      message = Message.new(@entity, @characters[:ned], :moveTo)
      message.data[:target] = Vector2.new(-200, 700)
      @messages.send << message
      message2 = Message.new(@entity, @characters[:player], :moveTo)
      message2.data[:target] = Vector2.new(500, 700)
      @messages.send << message2
    end
    
    if (@characters[:player].getComponent(PositionComponent).pos - Vector2.new(500, 700)).length < 300 and not @arrived
      @arrived = true
      @messages.send << Message.new(@entity, @characters[:player], :stop)
      @game.factory.createDialogWindow(@dialog2)
    end
    
    
    if @arrived and @dialog2.finished
      @messages.send << Message.new(@entity, @characters[:player], :unfreeze)
    end
  end
    
end