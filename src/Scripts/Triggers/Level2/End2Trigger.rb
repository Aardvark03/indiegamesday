require_relative '../../Script'

require_relative '../../../Utils/Message'
require_relative '../../../Utils/Vector2'


class End2Trigger < Script
  def initialize(entity, game)
    super(entity, game)
    @messages = @entity.getComponent(MessagingComponent)
  end
  
  def update(dt)
    @messages.recieved.each do |message|
      case message.type
        when :collision
          if message.data[:entity] == @game.currentLevel.characters[:player]
            @game.changeLevel(Level3)
          end
      end
    end
    @messages.recieved.clear
  end
end