require_relative '../Components/PositionComponent'
require_relative '../Components/CollisionComponent'
require_relative '../Nodes/CollisionNode'
require_relative '../EntitySystem/System'
require_relative '../Utils/Collision'
require_relative '../Utils/Vector2'
require_relative '../Utils/Message'

class CollisionSystem < System
  include Collision
  def initialize(world, game)
    super(world)
    @game = game
    @nodeList = @world.getNodeList(CollisionNode)
  end
  
  def init
  end
  
  def exit
  end
  
  def update(dt)
    @nodeList = @world.getNodeList(CollisionNode)
        
    @nodeList.nodes.each_value do |node|
      next if node.components[CollisionComponent].static
      neighbors = @world.grid.getNeighbors(node.entity)
      neighbors.each do |neighbor|
        next if node.entity == neighbor
        @node = node
        @neighbor = neighbor
        if node.components[CollisionComponent].type == :rect
          pos = node.components[PositionComponent].pos
          pos2 = neighbor.getComponent(PositionComponent).pos
          size = node.components[PositionComponent].size
          size2 = neighbor.getComponent(PositionComponent).size
  
          if not node.components[CollisionComponent].rect
            pos.x -= size.x*0.5
            pos.y -= size.y*0.5
          end
      
          if not neighbor.getComponent(CollisionComponent).rect
            pos2.x -= size2.x*0.5
            pos2.y -= size2.y*0.5
          end
          
          
          if intersectionRects(pos, size, pos2, size2)
            if node.components[CollisionComponent].massive and neighbor.getComponent(CollisionComponent).massive
              if not node.components[CollisionComponent].static
                @mov = node.entity.getComponent(MovementComponent)
                if @mov.nil?
                  raise "Not movable, but also not static"
                end
                moveOut(pos, size, pos2, size2)
              end
            else
              message = Message.new(:system, node.entity, :collision)
              message.data[:entity] = neighbor
              node.entity.getComponent(MessagingComponent).recieved << message
              message = Message.new(:system, neighbor, :collision)
              message.data[:entity] = node.entity
              neighbor.getComponent(MessagingComponent).recieved << message
            end
          end
          
          if not node.components[CollisionComponent].rect
            pos.x += size.x*0.5
            pos.y += size.y*0.5
          end
      
          if not neighbor.getComponent(CollisionComponent).rect
            pos2.x += size2.x*0.5
            pos2.y += size2.y*0.5
          end
          
          node.components[PositionComponent].pos = pos
          neighbor.getComponent(PositionComponent).pos = pos2
        end
      end
    end
  end

  
  def moveOut(pos1, size1, pos2, size2)
    
    posRect1 = pos1.dup
    sizeRect1 = size1.dup
    
    posRect2 = pos2
    sizeRect2 = size2
    
    mov1 = @node.entity.getComponent(MovementComponent)
    mov2 = @neighbor.getComponent(MovementComponent)
    
    # Check top
    posRect1.x += 0.3*sizeRect1.x
    sizeRect1.x *= 0.4
    sizeRect1.y *= 0.3
    
    
    
    if intersectionRects(posRect1, sizeRect1, posRect2, sizeRect2)
      pos1.y = pos2.y + size2.y
      
      sideEntity = :top
      sideVictim = :bottom
      
      mov1.v.y = 0
      mov2.v.y = 0 if not mov2.nil?
        
    end
     
    posRect1 = pos1.dup
    sizeRect1 = size1.dup
    
    # Check bottom
    posRect1.y += sizeRect1.y*0.7
    posRect1.x += 0.3*size1.x
    sizeRect1.y *= 0.3
    sizeRect1.x *= 0.4
    
    if intersectionRects(posRect1, sizeRect1, posRect2, sizeRect2)
      pos1.y = pos2.y-size1.y
      sideEntity = :bottom
      sideVictim = :top
      
      mov1.v.y = 0
      mov2.v.y = 0 if not mov2.nil?
      
    end
    
    posRect1 = pos1.dup
    sizeRect1 = size1.dup
    
    # Check left
    posRect1.y += sizeRect1.y*0.3
    sizeRect1.x *= 0.3
    sizeRect1.y *= 0.4
    
    if intersectionRects(posRect1, sizeRect1, posRect2, sizeRect2)
      pos1.x = posRect2.x + sizeRect2.x
      sideEntity = :left
      sideVictim = :right
      
      mov1.v.x = 0
      mov2.v.x = 0 if not mov2.nil?
    end
    
    posRect1 = pos1.dup
    sizeRect1 = size1.dup
    
    # Check right
    
    posRect1.x += 0.7*size1.x
    posRect1.y += sizeRect1.y*0.3
    sizeRect1.x *= 0.3
    sizeRect1.y *= 0.4
    
    if intersectionRects(posRect1, sizeRect1, posRect2, sizeRect2)
      pos1.x = posRect2.x-size1.x
      
      sideEntity = :right
      sideVictim = :left
      
      mov1.v.x = 0
      mov2.v.x = 0 if not mov2.nil?
    end
    
    if @node.entity.has?(MessagingComponent)
      collision = Message.new(:system, @node.entity, :collision)
      collision.data[:entity] = @neighbor
      collision.data[:side] = sideEntity
      @node.entity.getComponent(MessagingComponent).recieved << collision
    end
            
    if @neighbor.has?(MessagingComponent)
      collision = Message.new(:system, @neighbor, :collision)
      collision.data[:entity] = @node.entity
      collision.data[:side] = sideVictim
      @neighbor.getComponent(MessagingComponent).recieved << collision
    end  
  end
end