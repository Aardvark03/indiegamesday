require_relative '../Components/MessagingComponent'
require_relative '../Nodes/MessagingNode'

class MessagingSystem < System  
  def init
  end
  
  def exit
  end
  
  def update(dt)
    @nodeList = @world.getNodeList(MessagingNode)
    @nodeList.nodes.each_value do |node|
      messaging = node.getComponent(MessagingComponent)
      messaging.send.each do |message|
        if message.delay <= 0
          message.reciever.getComponent(MessagingComponent).recieved << message
          messaging.send.delete(message)
        else
          message.delay -= dt
        end
      end
    end
  end
end