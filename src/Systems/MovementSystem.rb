require 'gosu'

require_relative '../Utils/Vector2'
require_relative '../Utils/Collision'
require_relative '../Utils/Transform'
require_relative '../Nodes/MoveNode'
require_relative '../EntitySystem/System'
require_relative '../Components/MovementComponent'
require_relative '../Components/PositionComponent'

class MovementSystem < System
  include Collision
  include Transform
  
  attr_accessor :bounds, :side
  def initialize(world, game, window)
    super(world)
    @game = game
    @window = window
    @side = true
  end
  
  def init
    @nodeList = @world.getNodeList(MoveNode)
    @forcetweaker = 1
  end
  
  def exit
  end
  
  def update(dt)
    @bounds = @game.currentLevel.size
    @nodeList = @world.getNodeList(MoveNode)
    
    @nodeList.nodes.each_value do |node|
      @pos = node.getComponent(PositionComponent)
      @mov = node.getComponent(MovementComponent)
      @node = node
      
      move(node, dt)
    end
  end
  
  def move(node, dt)

    if @mov.target != :none
      @mov.f += calculate_force
    end
    
    @mov.f += Vector2.new(0, 300) if @side and not @mov.flying
    
    @mov.a = @mov.f/@mov.m
    @mov.f.x = 0
    @mov.f.y = 0
    
    @mov.v += @mov.a * dt
    @mov.a = Vector2.new(0, 0)
    
    if @side and not @mov.flying
      if @mov.v.x > @mov.v_max
        @mov.v.x = @mov.v_max
      elsif @mov.v.x < -@mov.v_max
        @mov.v.x = -@mov.v_max
      end
    else
      @mov.v.truncate(@mov.v_max)
    end
    
    rndr = node.getComponent(RenderComponent)
    if not rndr.nil?
      if rndr.rotate
        if @mov.v.length > 0.0000001 then
          @mov.dir = @mov.v.normalized
          if not (rndr = node.getComponent(RenderComponent)).nil? then
            angle = @mov.dir.getAngle
            rndr.angle = angle if not angle.nil?
          end     
        end
      end
    end    
    
    @pos.pos.x += @mov.v.x * dt
    @pos.pos.y += @mov.v.y * dt
    
    checkBounds
  end
  
  def calculate_force
    f = Vector2.new(0, 0)
    f += arrive
    f.truncate(2.0)
    return f * 200   # Tweaker
    
  end
  
  def checkBounds
    pos = @pos.pos
    if @mov.fixedBounds
      @pos.pos.x = 0 if pos.x < 0
      @pos.pos.x = @bounds.x-@pos.size.x if pos.x+@pos.size.x > @bounds.x
    
      @pos.pos.y = 0 if pos.y < 0
      @pos.pos.y = @bounds.y-@pos.size.y if pos.y+@pos.size.y > @bounds.y
    end
  end
  
  def arrive
    targetVector = @mov.target-@pos.pos
      
    dist = targetVector.length
          
    if dist > 0 then
      @decelerationTweaker = 0.3
      speed = dist/@decelerationTweaker
        
      
      speed = @mov.v_max if speed > @mov.v_max
        
      v_desired = targetVector*(speed/dist)
              
      return (v_desired - @mov.v)
        
    end
    Vector2.new(0, 0)
  end
end
