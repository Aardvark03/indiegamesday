require_relative '../Components/PositionComponent'
require_relative '../Components/RenderComponent'
require_relative '../Nodes/RenderNode'

class RenderSystem < System
  def initialize(world, game)
    super(world)
    @game = game
    @nodeList = @world.getNodeList(RenderNode)
    @normalFont = @game.assetMgr.getFont("Cantarell-Regular.ttf", 30)
    @bigFont = @game.assetMgr.getFont("Cantarell-Regular.ttf", 50)
    @window = @game.window
  end
  
  def init
  end
  
  def exit
  end
  
  def update(dt)
    @nodeList = @world.getNodeList(RenderNode)
    
    @renderCode = storeRenderCode do
      @nodeList.nodes.each_value do |node|
        rndr = node.getComponent(RenderComponent)
        pos = node.getComponent(PositionComponent)
      
        cameraPos = @game.camera.getComponent(PositionComponent)
        
        if not rndr.ui
          if pos.pos.x+pos.size.x * 0.5 < cameraPos.pos.x
            next
          elsif pos.pos.x - pos.size.x * 0.5 > cameraPos.pos.x + cameraPos.size.x
            next
          elsif pos.pos.y+pos.size.y * 0.5 < cameraPos.pos.y
            next
          elsif pos.pos.y - pos.size.y * 0.5 > cameraPos.pos.y+cameraPos.size.y
            next
          end
        end
        
        position = pos.pos.dup
        size = pos.size.dup
        
        if rndr.ui
          position += cameraPos.pos
        end
        
        if not rndr.fontSize.nil?
          case rndr.fontSize
            when :big
              @bigFont.draw(rndr.img, position.x, position.y, rndr.z, 1, 1, Gosu::Color.rgba(0x000000ff))
            when :normal
              @normalFont.draw(rndr.img, position.x, position.y, rndr.z, 1, 1, Gosu::Color.rgba(0x000000ff))
          end
            
        elsif rndr.img == :rect
          position -= size*0.5
          @game.window.draw_quad(position.x, position.y, Gosu::Color.rgba(0x00000064),
          position.x+size.x, position.y, Gosu::Color.rgba(0x00000064),
          position.x+size.x, position.y+size.y, Gosu::Color.rgba(0x00000064),
          position.x, position.y+size.y, Gosu::Color.rgba(0x00000064), rndr.z)
        else
          rndr.img.draw_rot(position.x, position.y, rndr.z, rndr.angle)
        end
      end
    end
  end
  
  def storeRenderCode(&block)
    return block
  end
  
  def render
    @renderCode.yield
  end
end