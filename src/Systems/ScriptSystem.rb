require_relative '../Components/ScriptComponent'
require_relative '../Nodes/ScriptNode'

class ScriptSystem < System  
  def init
  end
  
  def exit
  end
  
  def update(dt)
    @nodeList = @world.getNodeList(ScriptNode)
    
    @nodeList.nodes.each_value do |node|
      scriptComponent = node.getComponent(ScriptComponent)
      scriptComponent.scripts.each do |script|
        script.update(dt) if script.active
      end
    end
  end
end