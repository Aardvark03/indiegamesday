require 'gosu'

class AssetManager
  def initialize(window)
    @window = window
    @images = {}
    @fonts = {}
    @samples = {}
  end
  
  def getImage(path)
    path = "Assets/" + path
    return @images[path] if not @images[path].nil?
    @images[path] = Gosu::Image.new(@window, path, true)
  end
  
  def getFont(path, size=20)
    path = "Assets/" + path
    return @fonts[path+"#{size}"] if not @fonts[path+"#{size}"].nil?
    @fonts[path+"#{size}"] = Gosu::Font.new(@window, path, size)
  end
  
  def getSample(path)
    path = "Assets/Sounds/"+path
    return @samples[path] if not @samples[path].nil?
    @samples[path] = Gosu::Sample.new(@window, path)
  end
  
  def removeImage(image)
    @images.each do |path|
      if @images[path] == image
        @images.delete(path)
        return true
      end
    end
    return false
  end
  
  def removeFont(font)
    @fonts.each do |path|
      if @fonts[path] == font
        @fonts.delete(path)
        return true
      end
    end
    return false
  end
end
