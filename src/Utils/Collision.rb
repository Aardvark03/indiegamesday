require_relative 'Vector2'

module Collision
  def intersectionRects(posRect1, sizeRect1, posRect2, sizeRect2)
    return false if posRect1.x+sizeRect1.x < posRect2.x
    return false if posRect1.x > posRect2.x + sizeRect2.x
    return false if posRect1.y+sizeRect1.y < posRect2.y
    return false if posRect1.y > posRect2.y + sizeRect2.y
    true
  end

  def intersectionRectPoint(posRect, sizeRect, point)
    return false if posRect.x+sizeRect.x < point.x
    return false if posRect.x > point.x
    return false if posRect.y+sizeRect.y < point.y
    return false if posRect.y > point.y
    true
  end
end