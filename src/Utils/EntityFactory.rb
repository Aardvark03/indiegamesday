require 'gosu'

require_relative '../EntitySystem/Entity'
require_relative '../EntitySystem/World'
require_relative '../Utils/Vector2'

require_relative '../Components/MovementComponent'
require_relative '../Components/RenderComponent'
require_relative '../Components/PositionComponent'
require_relative '../Components/CollisionComponent'
require_relative '../Components/ScriptComponent'

require_relative '../Scripts/PlayerScript'

require_relative '../Scripts/SentenceScript'
require_relative '../Scripts/MouseScript'
require_relative '../Scripts/DialogScript'
require_relative '../Scripts/HumanoidScript'
require_relative '../Scripts/CameraScript'
require_relative '../Scripts/RabbitScript'
require_relative '../Scripts/SunScript'

require_relative '../../locals/Names'

class EntityFactory
  include Names
  def initialize(world, window, assetMgr, game)
    @world = world
    @window = window
    @assetMgr = assetMgr
    @game = game
  end
  
  def createPlayer(x, y)
    player = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = x
    pos.pos.y = y
    
    mov = MovementComponent.new
  
    mov.v_max = 200.0
    mov.m = 1.0
    mov.fixedBounds = true  
    
    player.addComponent(mov)
    
    rndr = RenderComponent.new(@assetMgr.getImage("player.png"))
    rndr.z = 5
      
    pos.size.x = rndr.img.width
    pos.size.y = rndr.img.height
    
    messaging = MessagingComponent.new
    player.addComponent(messaging)
      
    player.addComponent(rndr)
    player.addComponent(pos)
    
    coll = CollisionComponent.new
    player.addComponent(coll)
    
    scriptComponent = ScriptComponent.new
    
  
    pscript = PlayerScript.new(player, @game)
    scriptComponent.scripts << pscript
    scriptComponent.scripts << HumanoidScript.new(player, @game)
    player.addComponent(scriptComponent)
    
    @world.addEntity(player)
    return player
  end
  
  def createGround(y, width, height)
    ground = Entity.new
    
    position = PositionComponent.new
    position.pos = Vector2.new(0, y)
    position.size = Vector2.new(width, height)

    coll = CollisionComponent.new
    coll.static = true   
    coll.rect = true
    
    ground.addComponent(position)
    ground.addComponent(coll)
    
    @world.addEntity(ground)
    
    return ground
    
  end
  
  def createCamera(player)
    cam = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = 0
    pos.pos.y = 0
    pos.size = Vector2.new(1920.0, 1080.0)
    
    mov = MovementComponent.new
    mov.v_max = 300
    mov.m = 1.0
    mov.fixedBounds = true
    mov.flying = true
    
    cam.addComponent(mov)
    cam.addComponent(pos)
    
    script = ScriptComponent.new
    script.scripts << CameraScript.new(cam, @game)
    
    cam.addComponent(script)
    
    @world.addEntity(cam)
    return cam
  end
  
  def createSentence(x, y, text, speaker)
    string = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = x
    pos.pos.y = y
    
    rndr = RenderComponent.new(text)
    rndr.z = 9
    rndr.fontSize = :normal
    
    string.addComponent(pos)
    string.addComponent(rndr)
    
    scriptComponent = ScriptComponent.new
    sscript = SentenceScript.new(string, @game, speaker)
    scriptComponent.scripts << sscript
    
    string.addComponent(scriptComponent)
    
    @world.addEntity(string)
    
    return string
  end
  
  def createDialogWindow(dialog)
    dialogWindow = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = 1920.0*0.5
    pos.pos.y = 1080*0.8+1080*0.2*0.5
    pos.size.x = 1920
    pos.size.y = 1080*0.2
    
    rndr = RenderComponent.new(:rect)
    rndr.z = 9
    rndr.ui = true
    
    dialogWindow.addComponent(rndr)
    
    dialogWindow.addComponent(pos)
    
    script = ScriptComponent.new
    
    script.scripts << DialogScript.new(dialogWindow, @game, dialog)

    dialogWindow.addComponent(script)    
    @world.addEntity(dialogWindow)
    
    return dialogWindow
  end
  
  def createDialogText(x, y, width, text, name)
    dialogText = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = x
    pos.pos.y = y
    
    text = "#{name}: \n" + text
    
    rndr = RenderComponent.new(Gosu::Image.from_text(@window, text, "Assets/Cantarell-Regular.ttf", 30, 5, width.to_i, :left))
    rndr.ui = true
    pos.size.x = rndr.img.width
    pos.size.y = rndr.img.height
    rndr.z = 10
    
    dialogText.addComponent(pos)
    dialogText.addComponent(rndr)
    
    @world.addEntity(dialogText)
    
    return dialogText
  end
  
  def createMousePointer
    mouse = Entity.new
    
    pos = PositionComponent.new
    pos.pos = Vector2.new(0, 0)
    
    rndr = RenderComponent.new(@assetMgr.getImage("Mouse/inspect.png"))
    rndr.z = 15
        
    mouse.addComponent(pos)
    mouse.addComponent(rndr)
    
    scriptComponent = ScriptComponent.new

    scriptComponent.scripts << MouseScript.new(mouse, @game)
    
    mouse.addComponent(scriptComponent)
    
    @world.addEntity(mouse)
    return mouse
  end
  
  def createBGImage(x, y, img, z)
    image = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = x
    pos.pos.y = y
    
    rndr = RenderComponent.new(img)
    rndr.z = z
    
    pos.size.x = img.width
    pos.size.y = img.height
    
    image.addComponent(pos)
    image.addComponent(rndr)
    
    @world.addEntity(image)
    
    return image
  end
  
  def createBackground(path)
    x = 0
    y = 0
    tiles = Gosu::Image.load_tiles(@window, "Assets/"+path, -10, -10, true)
      
    tiles.each do |tile|
      part = Entity.new
      pos = PositionComponent.new
      if x > 9
        y+=1
        x=0
      end
      pos.pos.x = x * tile.width + 0.5 * tile.width
      pos.pos.y = y * tile.height + 0.5 * tile.height
      
      pos.size.x = tile.width
      pos.size.y = tile.height
      
      rndr = RenderComponent.new(tile)
      rndr.z = 0
      
      part.addComponent(pos)
      part.addComponent(rndr)
      @world.addEntity(part)
      x+=1
    end
    return Vector2.new(10*tiles[0].width, 10*tiles[0].height)
  end
  
  def createTrigger(x, y, width, height, scriptClass, type=:collision)
    trigger = Entity.new
    
    if type == :collision
      pos = PositionComponent.new
      pos.pos.x = x
      pos.pos.y = y
      pos.size.x = width
      pos.size.y = height
        
      coll = CollisionComponent.new
      coll.static = true
      coll.massive = false
      coll.rect = true
      
    end
    
    messaging = MessagingComponent.new
    
    trigger.addComponent(messaging)
    
    trigger.addComponent(pos)
    trigger.addComponent(coll)
    
    script = ScriptComponent.new
    script.scripts << scriptClass.new(trigger, @game)
    
    trigger.addComponent(script)
    @world.addEntity(trigger)
    
    return trigger
  end
  
  def createNPC(x, y, path, z)
    npc = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = x
    pos.pos.y = y
    
    messaging = MessagingComponent.new
    npc.addComponent(messaging)
    
    description = DescriptionComponent.new
    mov = MovementComponent.new
    script = ScriptComponent.new
    
    rndr = RenderComponent.new(@assetMgr.getImage(path))
    rndr.z = z
    npc.addComponent(rndr)
    npc.addComponent(description) 

    pos.size.x = rndr.img.width
    pos.size.y = rndr.img.height
    npc.addComponent(pos)
        
    mov.v_max = 200
    mov.m = 1.0
    mov.fixedBounds = false
    mov.flying = true
    mov.target = :none
    npc.addComponent(mov)
    
    npcScript = HumanoidScript.new(npc, @game)
    script.scripts << npcScript
    npc.addComponent(script)
   
    
    @world.addEntity(npc)
    
    return npc

    
  end
  
  def createSun(clouds)
    
    sun = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = 150
    pos.pos.y = 120
    
    rndr = RenderComponent.new(@assetMgr.getImage("Decoration/sun.png"))
    rndr.z = 1
    rndr.ui = true
    
    pos.size.x = rndr.img.width
    pos.size.y = rndr.img.height
    
    sun.addComponent(pos)
    sun.addComponent(rndr)
    
    script = ScriptComponent.new
    script.scripts << SunScript.new(sun, @game, clouds)

    sun.addComponent(script)
    
    @world.addEntity(sun)
    
    return sun
  end
  
  def createCloud(x, y, type)
    cloud = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = x
    pos.pos.y = y
    
    mov = MovementComponent.new
    
    mov.m = 1.0
    mov.fixedBounds = false
    mov.flying = true
    mov.target = Vector2.new(-300, pos.pos.y)
    
    case type
      when :normal
        rndr = RenderComponent.new(@assetMgr.getImage("Decoration/wolke3.png"))
        rndr.z = 2
        mov.v_max = 30
      when :small
        mov.v_max = 15
        rndr = RenderComponent.new(@assetMgr.getImage("Decoration/wolke2.png"))
        rndr.z = 1.5
      when :big
        rndr = RenderComponent.new(@assetMgr.getImage("Decoration/wolke1.png"))
        rndr.z = 2.5
        mov.v_max = 45
      else
        raise("THIS IS NO CLOUD SIZE!")
    end
    
    rndr.ui = true
        
    pos.size.x = rndr.img.width
    pos.size.y = rndr.img.height
   
    
    cloud.addComponent(mov)
    cloud.addComponent(pos)
    cloud.addComponent(rndr)
        
    @world.addEntity(cloud)
    
    return cloud
    
  end
    
    
  
  def createCreature(x, y, type)
    creature = Entity.new
    
    pos = PositionComponent.new
    pos.pos.x = x
    pos.pos.y = y
    
    messaging = MessagingComponent.new
    creature.addComponent(messaging)
    coll = CollisionComponent.new
    creature.addComponent(coll)
    
    description = DescriptionComponent.new
    mov = MovementComponent.new
    script = ScriptComponent.new
    
    case type
      when :rabbit
        rndr = RenderComponent.new(@assetMgr.getImage("Creatures/rabbit1.png"))
        rndr.z = 5
        creature.addComponent(rndr)
        description.name = Names::Rabbits.sample
        description.description = "A whimpy rabbit"
        description.action = :control
        creature.addComponent(description)    

        
        pos.size.x = rndr.img.width
        pos.size.y = rndr.img.height / 2.0
        creature.addComponent(pos)
        
        mov.v_max = 250
        mov.m = 1.0
        mov.fixedBounds = true
        mov.target = :none
        creature.addComponent(mov)
        
        script.scripts << RabbitScript.new(creature, @game)
    end
    
    creature.addComponent(script)
    
    @world.addEntity(creature)
    
    return creature
  end
end