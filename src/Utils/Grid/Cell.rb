require_relative '../../EntitySystem/Entity'
require_relative '../../Components/PositionComponent'

class Cell
  attr_accessor :entities, :pos, :size, :id
  @@next_id = 1
  def initialize(pos, size)
    @id = @@next_id
    @@next_id += 1
    @pos = pos
    @size = size
    @entities = []
  end
  
  def check(entity)
    return false if not entity.has?(PositionComponent)
    
    if inCell(entity)
      if @entities.index(entity).nil?
        @entities << entity
      end
      return true
    end
    false
  end
  
  def inCell(entity)
    ePos = entity.getComponent(PositionComponent).pos
    eSize = entity.getComponent(PositionComponent).size
    
    return false if ePos.x > @pos.x + @size.x
    return false if ePos.x + eSize.x < @pos.x
    return false if ePos.y > @pos.y + @size.y
    return false if ePos.y + eSize.y < @pos.y
    
    true
  end
  
  def contains?(entity)
    if @entities.index(entity)
      true
    else
      false
    end
  end
end