require_relative '../Vector2'
require_relative 'Cell'

class Grid
  attr_accessor :cells, :columns, :rows
  
  def initialize(size, columns, rows)
    @size = size
    @columns = columns
    @rows = rows
    
    @cellSize = Vector2.new(size.x/rows.to_f, size.y/columns.to_f)
    @cells = []
    
    x = 0
    y = 0
    
    columns.times do
      rows.times do 
        cell = Cell.new(Vector2.new(x*@cellSize.x, y*@cellSize.y), @cellSize)
        @cells << cell
        x += 1
      end
      y += 1
    end
        
  end
  
  def updateSize(newSize)
    @size = newSize
    
    @cellSize = Vector2.new(@size.x/rows.to_f, @size.y/columns.to_f)
    @cells = []
    
    x = 0
    y = 0
    
    @columns.times do
      @rows.times do 
        cell = Cell.new(Vector2.new(x*@cellSize.x, y*@cellSize.y), @cellSize)
        @cells << cell
        x += 1
      end
      x = 0
      y += 1
    end
  end
  
  def update(nodes)
    @cells.each {|cell| cell.entities.clear}
    nodes.each_value do |node|
      @cells.each do |cell|
        cell.check(node.entity)
      end
    end
  end
    
  def getNeighbors(entity)
    neighbors = []
    
    cells.each do |cell|
      if cell.contains?(entity)
        neighbors += cell.entities
      end
    end
    return neighbors.uniq
  end
  
  def check(entity)
    @cells.each do |cell|
      cell.check(entity)
    end
  end
end