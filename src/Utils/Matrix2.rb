class Matrix2 < Array
  
  def initialize
    self << [1.0, 0.0, 0.0]
    self << [0.0, 1.0, 0.0]
    self << [0.0, 0.0, 1.0]
  end
  
  
  def *(factor)
    result = Matrix2.new
    
    result[0][0] = self[0][0] * factor[0][0] + self[0][1] * factor[1][0] + self[0][2] * factor[2][0]
    result[0][1] = self[0][0] * factor[0][1] + self[0][1] * factor[1][1] + self[0][2] * factor[2][1]
    result[0][2] = self[0][0] * factor[0][2] + self[0][1] * factor[1][2] + self[0][2] * factor[2][2]
    
    result[1][0] = self[1][0] * factor[0][0] + self[1][1] * factor[1][0] + self[1][2] * factor[2][0]
    result[1][1] = self[1][0] * factor[0][1] + self[1][1] * factor[1][1] + self[1][2] * factor[2][1]
    result[1][2] = self[1][0] * factor[0][2] + self[1][1] * factor[1][2] + self[1][2] * factor[2][2]
    
    result[2][0] = self[2][0] * factor[0][0] + self[2][1] * factor[1][0] + self[2][2] * factor[2][0]
    result[2][1] = self[2][0] * factor[0][1] + self[2][1] * factor[1][1] + self[2][2] * factor[2][1]
    result[2][2] = self[2][0] * factor[0][2] + self[2][1] * factor[1][2] + self[2][2] * factor[2][2]
    
    return result
  end
  
  
  def translate(vec)
    t = Matrix2.new
    t[0][0] = 1; t[0][1] = 0; t[0][2] = 0
    t[1][0] = 0; t[1][1] = 1; t[1][2] = 0
    t[2][0] = vec.x; t[2][1] = vec.y; t[2][2] = 1
    
    return self*t
  end
  
  def rotate(forward, side)
    r = Matrix2.new
    
    r[0][0] = forward.x; r[0][1] = forward.y; r[0][2] = 0
    r[1][0] = side.x; r[1][1] = side.y; r[1][2] = 0
    r[2][0] = 0; r[2][1] = 0; r[2][2] = 1
    
    return self*r
    
  end
  
  def transformVector2(vec)
    x = self[0][0] * vec.x + self[1][0] * vec.y + self[2][0]
    y = self[0][1] * vec.x + self[1][1] * vec.y + self[2][1]
    
    return Vector2.new(x, y)
  end
end