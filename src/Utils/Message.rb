class Message
  attr_accessor :sender, :reciever, :type, :delay, :data
  
  def initialize(sender, reciever, type, delay=0.0)
    @sender = sender
    @reciever = reciever
    @type = type
    @delay = delay
    @data = {}
  end
end