require_relative 'Matrix2'
require_relative 'Vector2'

module Transform
  def pointToWorld(point, heading, side, pos)
    t = Matrix2.new
    t = t.rotate(heading, side)
    t = t.translate(pos)
    
    return t.transformVector2(point)
  end
  
  def pointToLocal(point, heading, side, pos) 
    t = Matrix2.new
    
    tx = -pos.dot(heading)
    ty = -pos.dot(side)
    
    t[0][0] = heading.x;  t[0][1] = side.x; t[0][2] = 0.0
    t[1][0] = heading.y;  t[1][1] = side.y; t[1][2] = 0.0
    t[2][0] = tx;         t[2][1] = ty;     t[2][2] = 0.0
    
   return t.transformVector2(point)
  end
end