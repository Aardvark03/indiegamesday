class Vector2
  attr_accessor :x, :y
  
  def initialize(x, y)
    @x = x
    @y = y
    
  end
  
  def +(summand)
    return Vector2.new(@x+summand, @y+summand) if summand.class == Fixnum || summand.class == Float
    return Vector2.new(@x+summand.x, @y+summand.y) if summand.class == Vector2
  end
  
  def -(subtrahend)
    return Vector2.new(@x-subtrahend, @y-subtrahend) if subtrahend.class == Fixnum || subtrahend.class == Float
    return Vector2.new(@x-subtrahend.x, @y-subtrahend.y) if subtrahend.class == Vector2
  end
  
  def *(factor)
    return Vector2.new(@x*factor, @y*factor) if factor.class == Fixnum || factor.class == Float
    return Vector2.new(@x*factor.x, @y*factor.y) if factor.class == Vector2
  end
  
  def /(divisor)
    return Vector2.new(@x/divisor, @y/divisor) if divisor.class == Fixnum || divisor.class == Float
    return Vector2.new(@x/divisor.x, @y/divisor.y) if divisor.class == Vector2
  end
  
  def truncate(max)
    if length > max then
        normalize
        @x *= max
        @y *= max
    end
      
  end
  
  
  def normalize
    l = length
    if l > 0.00001 then
      @x /= l
      @y /= l
    end
    0
  end
  
  def normalized
    l = length
    return Vector2.new(@x/l, @y/l) if l > 0.00001
    Vector2.new(0, 0)
  end
  
  def length
    Math.sqrt(@x*@x + @y*@y)
  end
  
  def getAngle
    rad = Math.atan2(@y, @x)
    angle = rad*180/Math::PI
  end
  
  def dot(vec)
    return self.x * vec.x + self.y * vec.y
  end
  
  def perp
    Vector2.new(-self.y, self.x)
  end
  
  
  
end