require 'gosu'
require 'singleton'

require_relative 'Utils/Vector2'
require_relative '../Configuration'
require_relative 'Game'

class App < Gosu::Window
  include Singleton
  include Configuration
  
  attr_accessor :resolution, :scalingFactor
  
  def initialize
    @resolution = Vector2.new(Configuration::X, Configuration::Y)
    
    super(@resolution.x, @resolution.y, Configuration::Fullscreen)
      
    @scalingFactor = @resolution.y / 1080.0
    
    @game = Game.new(self)

    
    @last_frame = Gosu::milliseconds
    
  end
  
  def update
    @this_frame = Gosu::milliseconds
    @dt = (@this_frame-@last_frame)/1000.0
    
    @game.update(@dt)
    #puts Gosu::fps
    
    @last_frame = @this_frame
  end
  
  def draw
    @game.render
  end
  
  def button_down(id)
    @game.button_down(id)
  end
  
end